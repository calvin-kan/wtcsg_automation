<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>yopmail_ResetPw</name>
   <tag></tag>
   <elementGuidId>ecc17df5-e351-43e2-970a-77fee9d0260d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>td > a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[text()='CLICK HERE TO RESET YOUR PASSWORD']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://infwtcsgy6uat.aswatson.net/login/pw/change?token=zLhiSn1l8u2KCne+a2ZVkwUrxGzHYcceI+8XMaLowGrRWLe%2FkBCwnkN4AYCf4C9GAGUF9DLwi4iiGzlyKtazPGooPFHPeUzgdQoQITLl+GmuUjcfXKpUbEoD3Com4l5BeXZ4355R96Bqo0REWYBv3NJTH1GicamgrlSRqa1RAZwSs1Y66M0t3q2y8wBFiYBX7aGtuS9Rj5kqWm8ZJRKVxA%3D%3D</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>rel</name>
      <type>Main</type>
      <value>nofollow</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>CLICK HERE TO RESET YOUR PASSWORD</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mailmillieu&quot;)/div[2]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[2]/table[1]/tbody[1]/tr[2]/td[1]/table[1]/tbody[1]/tr[1]/td[2]/table[1]/tbody[1]/tr[5]/td[1]/a[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/YopmailInbox/iframe_Headers_ifmail</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='mailmillieu']/div[2]/table/tbody/tr/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/a</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'CLICK HERE TO RESET YOUR PASSWORD')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('Did you forget your password? Don', &quot;'&quot;, 't worry, we can help you.')])[1]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Auto'])[1]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('Link doesn', &quot;'&quot;, 't work?')])[1]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please copy the link address to your browser.'])[1]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='CLICK HERE TO RESET YOUR PASSWORD']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://infwtcsgy6uat.aswatson.net/login/pw/change?token=zLhiSn1l8u2KCne+a2ZVkwUrxGzHYcceI+8XMaLowGrRWLe%2FkBCwnkN4AYCf4C9GAGUF9DLwi4iiGzlyKtazPGooPFHPeUzgdQoQITLl+GmuUjcfXKpUbEoD3Com4l5BeXZ4355R96Bqo0REWYBv3NJTH1GicamgrlSRqa1RAZwSs1Y66M0t3q2y8wBFiYBX7aGtuS9Rj5kqWm8ZJRKVxA%3D%3D')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/a</value>
   </webElementXpaths>
</WebElementEntity>

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.concurrent.ThreadLocalRandom as Keyword
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger

KeywordLogger logger = new KeywordLogger()

'Generate random number'
String randomNumber = String.valueOf(Math.abs(new Random().nextInt() % (9999999 - 1111111)) + 1111111)

'Declare strings'
String memberEmail = (('autotesting-' + randomNumber) + GlobalVariable.BU) + '@yopmail.com'

String memberFirstName = 'Auto Testing'

String memberLastName = 'Member'

String memberMobile = '9' + randomNumber

String addressFiller = 123456

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.defaultURL)

'Go to my account'
WebUI.enhancedClick(findTestObject('Home/home_MyAccountBtn'))

//Registration
'enhancedClick register button'
WebUI.enhancedClick(findTestObject('Login/login_RegisterButton'))

'click sign up as member'
WebUI.enhancedClick(findTestObject('Registration/registration_signUpAsMember'))

'Enter name with randomly generated number'
WebUI.setText(findTestObject('Registration/registration_member_firstName'), memberFirstName)

'Enter name with randomly generated number'
WebUI.setText(findTestObject('Registration/registration_member_lastName'), memberLastName)

'open birth year dropdown'
WebUI.enhancedClick(findTestObject('Registration/registration_BirthYear'))

'click the year'
WebUI.enhancedClick(findTestObject('Registration/registration_BirthYear1900'))

'open birth month dropdown'
WebUI.enhancedClick(findTestObject('Registration/registration_BirthMonth'))

'click the month'
WebUI.enhancedClick(findTestObject('Registration/registration_BirthMonth1'))

'Paste email'
WebUI.setText(findTestObject('Registration/registraion_EmailField'), memberEmail)

'Paste email again'
WebUI.setText(findTestObject('Registration/registration_EmailVerificationField'), memberEmail)

'Enter mobile'
WebUI.setText(findTestObject('Registration/registration_MobileField'), memberMobile)

'Enter pw'
WebUI.setText(findTestObject('Registration/registration_PasswordField'), GlobalVariable.defaultPassword)

'Enter pw again'
WebUI.setText(findTestObject('Registration/registration_PwVerificationField'), GlobalVariable.defaultPassword)

'enter house number'
WebUI.setText(findTestObject('Registration/registration_HouseNo'), addressFiller)

'enter street name'
WebUI.setText(findTestObject('Registration/registration_StreetName'), addressFiller)

'enter unit number'
WebUI.setText(findTestObject('Registration/registration_UnitNo'), addressFiller)

'enter post code'
WebUI.setText(findTestObject('Registration/registration_PostalCode'), addressFiller)

'submit'
WebUI.enhancedClick(findTestObject('Registration/registration_SubmitButton'))

'enter OTP'
WebUI.setText(findTestObject('Registration/registration_otp'), GlobalVariable.defaultOTP)

'click submit otp'
WebUI.enhancedClick(findTestObject('Registration/registration_submitOTP'), FailureHandling.STOP_ON_FAILURE)

'click authorize checkbox'
WebUI.enhancedClick(findTestObject('Checkout/checkout_autherizeChkbox'))

'click pay button'
WebUI.enhancedClick(findTestObject('Checkout/checkout_PayBtn'))

'click confirm personal info button'
WebUI.enhancedClick(findTestObject('PaymentGateway/paymentGateway_confirmPerInfoBtn'))

'select visa'
WebUI.enhancedClick(findTestObject('PaymentGateway/paymentGateway_visaCardTypeRdBtn'))

'enter card number'
WebUI.setText(findTestObject('PaymentGateway/paymentGateway_cardNo'), GlobalVariable.visaCard_cardNo)

'enter expiry month'
WebUI.selectOptionByValue(findTestObject('PaymentGateway/paymentGateway_expiryMonth'), GlobalVariable.visaCard_expiryMonth, 
    false)

'enter expiry year'
WebUI.selectOptionByValue(findTestObject('PaymentGateway/paymentGateway_expiryYear'), GlobalVariable.visaCard_expiryYear, 
    false)

'enter cvv'
WebUI.sendKeys(findTestObject('PaymentGateway/paymentGateway_cvv'), GlobalVariable.visaCard_cvv)

'click confirm card details button'
WebUI.enhancedClick(findTestObject('PaymentGateway/paymentGateway_confirmCardDetails'))

'click pay'
WebUI.enhancedClick(findTestObject('PaymentGateway/paymentGateway_payBtn'))

'enter visa otp'
WebUI.setText(findTestObject('PaymentGateway/paymentGateway_visaPw'), GlobalVariable.visaCard_otp)

'submit otp'
WebUI.enhancedClick(findTestObject('PaymentGateway/paymentGateway_submitVisaPw'))

'check thank you page heading visible'
WebUI.verifyTextPresent(GlobalVariable.thankYouPageHeading, false)

'verify element present : account summary title'
WebUI.verifyElementPresent(findTestObject('AccountSummary/account_AccountSummaryTitle'), 3)

'change pw'
WebUI.enhancedClick(findTestObject('AccountSummary/account_changePw'))

'enter old pw'
WebUI.setText(findTestObject('ChangePassword/changePw_oldPw'), GlobalVariable.defaultPassword)

'enter new pw'
WebUI.setText(findTestObject('ChangePassword/changePw_newPw'), GlobalVariable.changePassword)

'enter new pw again'
WebUI.setText(findTestObject('ChangePassword/changePw_confirmNewPw'), GlobalVariable.changePassword)

'click update pw'
WebUI.enhancedClick(findTestObject('ChangePassword/changePw_updatePwBtn'))

'verify element present : pw updated message'
WebUI.verifyElementVisible(findTestObject('ChangePassword/changePw_PwUpdatedMsg'))

'mouse over the my account btn'
WebUI.mouseOver(findTestObject('Home/home_MyAccountBtn'))

'click the logout btn'
WebUI.enhancedClick(findTestObject('Home/home_logoutBtn'))

'click my account'
WebUI.enhancedClick(findTestObject('Home/home_MyAccountBtn'))

'enter login email'
WebUI.setText(findTestObject('Login/login_usernameField'), memberEmail)

'enter pw'
WebUI.setText(findTestObject('Login/login__passwordField'), GlobalVariable.changePassword)

'click login btn'
WebUI.enhancedClick(findTestObject('Login/login_loginButton'))

'mouse over the my account btn'
WebUI.mouseOver(findTestObject('Home/home_MyAccountBtn'))

'click the logout btn'
WebUI.enhancedClick(findTestObject('Home/home_logoutBtn'))

'click my account'
WebUI.enhancedClick(findTestObject('Home/home_MyAccountBtn'))

'forget pw'
WebUI.enhancedClick(findTestObject('Login/login_forgetPw'))

'click reset pw by email option'
WebUI.enhancedClick(findTestObject('ForgetPassword/forgetPw_emailResetOption'))

'click next'
WebUI.enhancedClick(findTestObject('ForgetPassword/forgetPw_nextBtn'))

'set loop flag to false'
Boolean whileLoopRanOnce = false

'set email received flag to false'
Boolean resetPwEmailReceived = false

'run loop as long as email is not received'
while (resetPwEmailReceived == false) {
    'run if loop flag is true'
    if (whileLoopRanOnce == true) {
        'go to reset pw page'
        WebUI.navigateToUrl('https://infwtcsgy6uat.aswatson.net/login/pw/forgetByEmail')
    }
    
    'enter email '
    WebUI.setText(findTestObject('ForgetPassword/forgetPw_emailField'), memberEmail)

    'confirm email'
    WebUI.enhancedClick(findTestObject('ForgetPassword/forgetPw_confirmEmailBtn'))

    'verify element present : reset pw email has been sent popup'
    WebUI.verifyTextPresent(GlobalVariable.pwResetToEmailMsg, false)

    'wait 10 sec'
    WebUI.delay(10)

    'go to yopmail'
    WebUI.navigateToUrl('http://www.yopmail.com/en/')

    'enter email'
    WebUI.setText(findTestObject('YopmailHomePage/yopmail_EmailField'), memberEmail)

    'click check email button'
    WebUI.enhancedClick(findTestObject('YopmailHomePage/yopmail_CheckInboxBtn'))

    'click the link in the email'
    resetPwEmailReceived = WebUI.verifyElementPresent(findTestObject('YopmailInbox/yopmail_ResetPw'), 3, FailureHandling.OPTIONAL)

    'change loop flag to true'
    whileLoopRanOnce = true
}

'click reset pw link in the email'
WebUI.enhancedClick(findTestObject('YopmailInbox/yopmail_ResetPw'))

'switch to new tab'
WebUI.switchToWindowIndex(1)

'enter new pw'
WebUI.setText(findTestObject('CreateNewPassword/newPw_newPw'), GlobalVariable.defaultPassword)

'enter new pw again'
WebUI.setText(findTestObject('CreateNewPassword/newPw_ConfirmPw'), GlobalVariable.defaultPassword)

'click next'
WebUI.enhancedClick(findTestObject('CreateNewPassword/newPw_Next'))

'verify element present : change pw successful msg'
WebUI.verifyTextPresent(GlobalVariable.changePwSucessfulMsg, false)

'verify element present : the email is correct'
WebUI.verifyTextPresent(memberEmail, false)

'click login now'
WebUI.enhancedClick(findTestObject('CreateNewPassword/newPw_LoginNowBtn'))

'fill in login'
WebUI.setText(findTestObject('Login/login_usernameField'), memberEmail)

'fill in pw'
WebUI.setText(findTestObject('Login/login__passwordField'), GlobalVariable.defaultPassword)

'click login btn'
WebUI.click(findTestObject('Login/login_loginButton'))

'check if reached ac summary page'
WebUI.verifyElementText(findTestObject('AccountSummary/account_AccountSummaryTitle'), GlobalVariable.acSummary_Heading)

'write email to log'
logger.logInfo(memberEmail)


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.defaultURL)

'click store locator'
WebUI.enhancedClick(findTestObject('Home/home_StoreLocator'))

'verify element present : store locator heading'
WebUI.verifyElementText(findTestObject('StoreLocator/storeLocator_Heading'), GlobalVariable.storeLocator_Heading)

'wait 6 sec for map to load'
WebUI.delay(6)

'scroll to check map button'
WebUI.scrollToElement(findTestObject('StoreLocator/storeLocator_CheckMapBtn'), 3)

'click check map button'
WebUI.enhancedClick(findTestObject('StoreLocator/storeLocator_CheckMapBtn'))

'click more details button'
WebUI.enhancedClick(findTestObject('StoreLocator/storeLocator_MoreDetailsBtn'))

'verify element present : store heading'
WebUI.verifyTextPresent(GlobalVariable.storeLocator_StoreHeading, false)


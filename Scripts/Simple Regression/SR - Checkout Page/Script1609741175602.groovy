import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Generate random number'
String eShopperFirstNameEnglish = 'Auto Testing'

String eShopperLastNameEnglish = 'ENG eShopper'

String eShopperMobile = String.valueOf(Math.abs(new Random().nextInt() % (99999999 - 11111111)) + 11111111)

String addressFiller = '431201'

'check elements visible'
WebUI.verifyElementVisible(findTestObject('Checkout/checkout_deliveryInfoSection'), FailureHandling.CONTINUE_ON_FAILURE)

'verify element present : delivery options section'
WebUI.verifyElementVisible(findTestObject('Checkout/checkout_deliveryOptionsSection'), FailureHandling.CONTINUE_ON_FAILURE)

'verify element present : voucher section'
WebUI.verifyElementVisible(findTestObject('Checkout/checkout_codeVoucherSection'), FailureHandling.CONTINUE_ON_FAILURE)

'verify element present : payment method section'
WebUI.verifyElementVisible(findTestObject('Checkout/Payment Method/checkout_paymentMethodSection'), FailureHandling.CONTINUE_ON_FAILURE)

'verify element present : pay button'
WebUI.verifyElementVisible(findTestObject('Checkout/checkout_PayBtn'), FailureHandling.CONTINUE_ON_FAILURE)

'Enter name with randomly generated number'
WebUI.setText(findTestObject('Checkout/HD - New Address/checkout_newAddress_firstName'), eShopperFirstNameEnglish)

'enter last name'
WebUI.setText(findTestObject('Checkout/HD - New Address/checkout_newAddress_lastName'), eShopperLastNameEnglish)

'Enter SG mobile'
WebUI.setText(findTestObject('Checkout/HD - New Address/checkout_newAddress_mobile'), eShopperMobile)

'enter house number'
WebUI.setText(findTestObject('Checkout/HD - New Address/checkout_newAddress_HouseNo'), addressFiller)

'enter street name'
WebUI.setText(findTestObject('Checkout/HD - New Address/checkout_newAddress_streetName'), addressFiller)

'enter unit number'
WebUI.setText(findTestObject('Checkout/HD - New Address/checkout_newAddress__unitNo'), addressFiller)

'enter post code'
WebUI.setText(findTestObject('Checkout/HD - New Address/checkout_newAddress__postCode'), addressFiller)

'click add to address checkbox'
WebUI.enhancedClick(findTestObject('Checkout/HD - New Address/checkout_newAddress_AddToAddressBkChkbox'))

'click save and ship'
WebUI.enhancedClick(findTestObject('Checkout/HD - New Address/checkout_newAddress_Save and Ship to this address'))

'verify address has been added'
WebUI.verifyTextPresent(eShopperFirstNameEnglish, false)

WebUI.verifyTextPresent(eShopperMobile, false)

WebUI.verifyTextPresent(addressFiller, false)


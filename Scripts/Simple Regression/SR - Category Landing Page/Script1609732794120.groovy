import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.subCatURL)

'verify element present : sub category filter'
WebUI.verifyElementVisible(findTestObject('SubCategoryPage/subCat_CateogryFilter'), FailureHandling.CONTINUE_ON_FAILURE)

'verify element present : sub category PLP filter'
WebUI.verifyElementVisible(findTestObject('SubCategoryPage/subCat_PLPFilter'), FailureHandling.CONTINUE_ON_FAILURE)

'verify element present : default sorting is best seller'
WebUI.verifyElementVisible(findTestObject('SubCategoryPage/subCat_PLPFIlterDefaultSorting'), FailureHandling.CONTINUE_ON_FAILURE)

'verify element present : PLP'
WebUI.verifyElementVisible(findTestObject('SubCategoryPage/subCat_PLP'), FailureHandling.CONTINUE_ON_FAILURE)


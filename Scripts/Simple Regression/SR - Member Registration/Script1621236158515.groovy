import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.concurrent.ThreadLocalRandom as Keyword
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger

KeywordLogger logger = new KeywordLogger()

'Generate random number'
String randomNumber = String.valueOf(Math.abs(new Random().nextInt() % (9999999 - 1111111)) + 1111111)

'Declare strings'
String memberEmail = (('autotesting-' + randomNumber) + GlobalVariable.BU) + '@yopmail.com'

String memberFirstName = 'Auto Testing'

String memberLastName = 'Member'

String memberMobile = '9' + randomNumber

String addressFiller = 123456

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.defaultURL)

'Go to my account'
WebUI.enhancedClick(findTestObject('Home/home_MyAccountBtn'))

//Registration
'enhancedClick register button'
WebUI.enhancedClick(findTestObject('Login/login_RegisterButton'))

'click sign up as member'
WebUI.enhancedClick(findTestObject('Registration/registration_signUpAsMember'))

'Enter name with randomly generated number'
WebUI.setText(findTestObject('Registration/registration_member_firstName'), memberFirstName)

'Enter name with randomly generated number'
WebUI.setText(findTestObject('Registration/registration_member_lastName'), memberLastName)

'open birth year dropdown'
WebUI.enhancedClick(findTestObject('Registration/registration_BirthYear'))

'click the year'
WebUI.enhancedClick(findTestObject('Registration/registration_BirthYear1900'))

'open birth month dropdown'
WebUI.enhancedClick(findTestObject('Registration/registration_BirthMonth'))

'click the month'
WebUI.enhancedClick(findTestObject('Registration/registration_BirthMonth1'))

'Paste email'
WebUI.setText(findTestObject('Registration/registraion_EmailField'), memberEmail)

'Paste email again'
WebUI.setText(findTestObject('Registration/registration_EmailVerificationField'), memberEmail)

'Enter mobile'
WebUI.setText(findTestObject('Registration/registration_MobileField'), memberMobile)

'Enter pw'
WebUI.setText(findTestObject('Registration/registration_PasswordField'), GlobalVariable.defaultPassword)

'Enter pw again'
WebUI.setText(findTestObject('Registration/registration_PwVerificationField'), GlobalVariable.defaultPassword)

'enter house number'
WebUI.setText(findTestObject('Registration/registration_HouseNo'), addressFiller)

'enter street name'
WebUI.setText(findTestObject('Registration/registration_StreetName'), addressFiller)

'enter unit number'
WebUI.setText(findTestObject('Registration/registration_UnitNo'), addressFiller)

'enter post code'
WebUI.setText(findTestObject('Registration/registration_PostalCode'), addressFiller)

'submit'
WebUI.enhancedClick(findTestObject('Registration/registration_SubmitButton'))

'enter OTP'
WebUI.setText(findTestObject('Registration/registration_otp'), GlobalVariable.defaultOTP)

'click submit otp'
WebUI.enhancedClick(findTestObject('Registration/registration_submitOTP'), FailureHandling.STOP_ON_FAILURE)

'click authorize checkbox'
WebUI.enhancedClick(findTestObject('Checkout/checkout_autherizeChkbox'))

'click pay button'
WebUI.enhancedClick(findTestObject('Checkout/checkout_PayBtn'))

'click confirm personal info button'
WebUI.enhancedClick(findTestObject('PaymentGateway/paymentGateway_confirmPerInfoBtn'))

'select visa'
WebUI.enhancedClick(findTestObject('PaymentGateway/paymentGateway_visaCardTypeRdBtn'))

'enter card number'
WebUI.setText(findTestObject('PaymentGateway/paymentGateway_cardNo'), GlobalVariable.visaCard_cardNo)

'enter expiry month'
WebUI.selectOptionByValue(findTestObject('PaymentGateway/paymentGateway_expiryMonth'), GlobalVariable.visaCard_expiryMonth, 
    false)

'enter expiry year'
WebUI.selectOptionByValue(findTestObject('PaymentGateway/paymentGateway_expiryYear'), GlobalVariable.visaCard_expiryYear, 
    false)

'enter cvv'
WebUI.sendKeys(findTestObject('PaymentGateway/paymentGateway_cvv'), GlobalVariable.visaCard_cvv)

'click confirm card details button'
WebUI.enhancedClick(findTestObject('PaymentGateway/paymentGateway_confirmCardDetails'))

'click pay'
WebUI.enhancedClick(findTestObject('PaymentGateway/paymentGateway_payBtn'))

'enter visa otp'
WebUI.setText(findTestObject('PaymentGateway/paymentGateway_visaPw'), GlobalVariable.visaCard_otp)

'submit otp'
WebUI.enhancedClick(findTestObject('PaymentGateway/paymentGateway_submitVisaPw'))

'check thank you page heading visible'
WebUI.verifyTextPresent(GlobalVariable.thankYouPageHeading, false)

'write email to log'
logger.logInfo(memberEmail)


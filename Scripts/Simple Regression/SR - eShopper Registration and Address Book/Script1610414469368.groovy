import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.concurrent.ThreadLocalRandom as Keyword
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger

'Generate random number'
String randomNumber = String.valueOf(Math.abs(new Random().nextInt() % (999999 - 111111)) + 111111)

'Declare strings'
String eShopperEmail = (('autotesting-' + randomNumber) + GlobalVariable.BU) + '@yopmail.com'

String eShopperFirstNameEnglish = 'Auto Testing'

String eShopperLastNameEnglish = 'ENG eShopper'

String eShopperMobile = '09' + randomNumber

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.defaultURL)

'Go to my account'
WebUI.enhancedClick(findTestObject('Home/home_MyAccountBtn'))

//Registration
'click register button'
WebUI.enhancedClick(findTestObject('Login/login_RegisterButton'))

'Enter name with randomly generated number'
WebUI.setText(findTestObject('Registration/registration_english_firstName'), eShopperFirstNameEnglish)

'Enter name with randomly generated number'
WebUI.setText(findTestObject('Registration/registration_english_lastName'), eShopperLastNameEnglish)

'Paste email'
WebUI.sendKeys(findTestObject('Registration/registration_eShopperEmailField'), eShopperEmail)

'Paste email again'
WebUI.sendKeys(findTestObject('Registration/registration_eShopperEmailVerField'), eShopperEmail)

'Enter mobile'
WebUI.sendKeys(findTestObject('Registration/registration_eShopperMobileField'), eShopperMobile)

'Enter pw'
WebUI.sendKeys(findTestObject('Registration/registration_eShopperPwField'), GlobalVariable.defaultPassword)

'Enter pw again'
WebUI.sendKeys(findTestObject('Registration/registration_eShopperPwVerField'), GlobalVariable.defaultPassword)

'submit'
WebUI.enhancedClick(findTestObject('Registration/registration_SubmitButton'))

'check registration successful'
WebUI.verifyTextPresent(GlobalVariable.reg_Success_Welcome, false)

WebUI.verifyTextPresent(GlobalVariable.reg_Success_eShopperText, false)

'Print email and pw to log'
println(((('\n\n####\nEmail : ' + eShopperEmail) + '\nPassword : ') + GlobalVariable.defaultPassword) + '\n####')

//Address Book
WebUI.enhancedClick(findTestObject('Home/home_MyAccountBtn'))

'click the address book link'
WebUI.enhancedClick(findTestObject('AccountSummary/account_AddressBook'))

'add address'
WebUI.enhancedClick(findTestObject('AddressBook/addressBook_AddAddress'))

'Enter name with randomly generated number'
WebUI.setText(findTestObject('AddressBook/addressBook_FirstNameField'), eShopperFirstNameEnglish)

WebUI.setText(findTestObject('AddressBook/addressBook_LastNameField'), eShopperLastNameEnglish)

'Enter mobile'
WebUI.setText(findTestObject('AddressBook/addressBook_MobileField'), eShopperMobile)

'enter house number'
WebUI.setText(findTestObject('AddressBook/addressBook_HouseNo'), randomNumber)

'enter street name'
WebUI.setText(findTestObject('AddressBook/addressBook_StreetName'), randomNumber)

'enter unit number'
WebUI.setText(findTestObject('AddressBook/addressBook_UnitNo'), randomNumber)

'enter post code'
WebUI.setText(findTestObject('AddressBook/addressBook_PostCode'), randomNumber)

'click update address button'
WebUI.enhancedClick(findTestObject('AddressBook/addressBook_UpdateAddressBtn'))

'check the name was displayed correctly'
WebUI.verifyTextPresent(eShopperFirstNameEnglish, false)

'check the mobile is displayed correctly'
WebUI.verifyTextPresent(eShopperMobile, false)

'edit address'
WebUI.click(findTestObject('AddressBook/addressBook_EditAddressBtn'))

'edit street name again'
WebUI.setText(findTestObject('AddressBook/addressBook_StreetName'), randomNumber + ' edited')

'update address'
WebUI.enhancedClick(findTestObject('AddressBook/addressBook_UpdateAddressBtn'))

'verify the edit'
WebUI.verifyTextPresent('edited', false)

'delete address'
WebUI.enhancedClick(findTestObject('AddressBook/addressBook_DeleteAddressBtn'))

'make sure the address has been deleted'
WebUI.verifyTextNotPresent(eShopperMobile, false)

'write email into log'
KeywordLogger logger = new KeywordLogger()

logger.logInfo(eShopperEmail)


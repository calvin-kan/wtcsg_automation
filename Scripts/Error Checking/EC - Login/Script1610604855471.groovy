import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

'go to homepage'
WebUI.navigateToUrl(GlobalVariable.defaultURL)

WebUI.click(findTestObject('Home/home_MyAccountBtn'))

'click login to trigger error messages'
WebUI.click(findTestObject('Login/login_loginButton'))

WebUI.verifyElementText(findTestObject('Login/errors_Login_EmptyEmail'), GlobalVariable.globalError_Login_EmptyEmail)

WebUI.verifyElementText(findTestObject('Login/errors_Login_EmptyPw'), GlobalVariable.globalError_Login_EmptyPw)

WebUI.setText(findTestObject('Login/login_usernameField'), 'wrongemail')

WebUI.setText(findTestObject('Login/login__passwordField'), 'wrongpw')

'click login to trigger error messages'
WebUI.click(findTestObject('Login/login_loginButton'))

WebUI.verifyElementText(findTestObject('Login/errors_Login_AccountNotExists'), GlobalVariable.globalError_Login_AccountNotExists)


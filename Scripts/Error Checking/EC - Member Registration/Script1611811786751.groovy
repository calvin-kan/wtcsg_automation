import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.concurrent.ThreadLocalRandom as Keyword

String addressFiller = 123456

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.defaultURL)

'Go to my account'
WebUI.click(findTestObject('Home/home_MyAccountBtn'))

'click register button'
WebUI.click(findTestObject('Login/login_RegisterButton'))

WebUI.enhancedClick(findTestObject('Registration/registration_signUpAsMember'))

'click to trigger errors'
WebUI.click(findTestObject('Registration/registration_SubmitButton'))

WebUI.verifyElementText(findTestObject('Registration/Error Checking/errors_registration_Email'), GlobalVariable.globalError_ManFieldMissing)

WebUI.verifyElementText(findTestObject('Registration/Error Checking/errors_registration_EmailVer'), GlobalVariable.globalError_ManFieldMissing)

WebUI.verifyElementText(findTestObject('Registration/Error Checking/errors_registration_Mobile'), GlobalVariable.globalError_ManFieldMissing)

WebUI.verifyElementText(findTestObject('Registration/Error Checking/errors_Registration_Pw'), GlobalVariable.globalError_ManFieldMissing)

WebUI.verifyElementText(findTestObject('Registration/Error Checking/errors_Registration_PwVer'), GlobalVariable.globalError_ManFieldMissing)

WebUI.sendKeys(findTestObject('Registration/registration_member_firstName'), 'firstName')

WebUI.sendKeys(findTestObject('Registration/registration_member_lastName'), 'lastName')

WebUI.sendKeys(findTestObject('Registration/registraion_EmailField'), 'wrongemail')

WebUI.sendKeys(findTestObject('Registration/registration_EmailVerificationField'), 'wrongwrongemail')

WebUI.enhancedClick(findTestObject('Registration/registration_BirthYear'))

WebUI.enhancedClick(findTestObject('Registration/registration_BirthYear1900'))

WebUI.enhancedClick(findTestObject('Registration/registration_BirthMonth'))

WebUI.enhancedClick(findTestObject('Registration/registration_BirthMonth1'))

WebUI.sendKeys(findTestObject('Registration/registration_MobileField'), 'wrongmobile')

WebUI.sendKeys(findTestObject('Registration/registration_PasswordField'), 'wrongpw')

WebUI.sendKeys(findTestObject('Registration/registration_PwVerificationField'), 'wrongpassword')

WebUI.setText(findTestObject('Registration/registration_HouseNo'), addressFiller)

WebUI.setText(findTestObject('Registration/registration_StreetName'), addressFiller)

WebUI.setText(findTestObject('Registration/registration_UnitNo'), addressFiller)

WebUI.setText(findTestObject('Registration/registration_PostalCode'), addressFiller)

'Click to trigger further errors'
WebUI.click(findTestObject('Registration/registration_SubmitButton'))

WebUI.verifyElementText(findTestObject('Registration/Error Checking/errors_Registration_EmailFormat'), GlobalVariable.globalError_WrongEmailFormat)

WebUI.verifyElementText(findTestObject('Registration/Error Checking/errors_Registration_EmailMismatch'), GlobalVariable.globalError_EmailMismatch)

WebUI.verifyElementText(findTestObject('Registration/Error Checking/errors_Registration_MobileFormat'), GlobalVariable.globalError_WrongMobileFormat)

WebUI.verifyElementText(findTestObject('Registration/Error Checking/errors_Registration_PwFormat'), GlobalVariable.globalError_PwFormat)


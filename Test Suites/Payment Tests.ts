<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Payment Tests</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>572d7e94-ac7f-40bf-8e5e-b88000ca3c17</testSuiteGuid>
   <testCaseLink>
      <guid>30169f17-5b43-4c06-a828-68dd80883edb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/PT - Member Login</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d62ffa29-a6b7-4fee-924f-072860b00619</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>79cd5624-bce5-4f1c-a970-4507589cf37d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3ae33739-7620-4d56-8aab-c893235e0ebf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/HD/PT - HD - Visa</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d78f1ec5-a5af-4093-acc7-211277e4988e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/HD/PT - HD - AE</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1dc4134b-fc7b-43b2-9ea5-a8ae4d644c85</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CC/PT - CCS - Visa</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c74be1da-0879-4dab-b7ac-49ef24891336</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CC/PT - CCS - AE</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>69454d13-e47e-4813-8597-61374ae8be9b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/HD/PT - HD - SDD - Visa</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b284f0f0-7f17-4dd3-9a1f-befc78d0ca55</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/HD/PT - HD - SDD - AE</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f7e2ad8a-e1b6-4b91-a278-55e423e5c58b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/FD/PT - FD - Visa</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7804a188-b6c3-46a8-b64b-bd512caa222e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/FD/PT - FD - AE</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>339b7ffa-011a-47b9-a2d5-94e8f5d34344</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/PT - Display Order Numbers</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
